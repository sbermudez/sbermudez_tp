<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"> <!--Uso UTF-8-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="icon" href="./Imágenes/IFTS_icono.ico" type="image/x-icon"> <!--Icono en la pestaña-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="includes/estilos.css" rel="stylesheet" type="text/css"> <!--Mira la hoja de estilos CSS-->
    <title>IFTS4 - Home</title>
</head>
<body style="background-color: white;">
    <header style="background-image: url(./Imágenes/Cabecera2.jpg);">
        <a href="./inicio.php"><img style="padding-left:20px;" src="./Imágenes/IFTS_logo.jpg"></a>
    </header>
    <hr>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <table width="100%" frame="border">
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarAlumnos.php" style="text-decoration: none;color: #5B7354;">Alumnos</a></td></h2>
            </td>
            <td width ="25%">
                <h2 style="text-align: center;"><a href="./ListarCarreras.php" style="text-decoration: none; color: #5B7354">Carreras</a></h2>
            </td>
            <td width="25%" >
                <h2 style="text-align: center;"><a href="./ListarMaterias.php" style="text-decoration: none; color: #5B7354">Materias</a></h2>
            </td>
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarInscripciones.php" style="text-decoration: none;color: #5B7354;">Inscripciones</a></td></h2>
            </td>
        </table>
    </nav>

    <hr>
    <h1>Inicio</h1>
    <article>
        <table cellpadding="40" cellspacing = "10" frame="border">
            <td style="text-align: justify;" width="33%">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime dolorem velit laudantium unde aut nam quaerat rerum, quam cumque quos eveniet impedit molestias, expedita reprehenderit necessitatibus magnam reiciendis doloremque soluta.
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Consequatur dignissimos earum voluptate, enim veniam inventore nobis commodi fugiat dolorum adipisci soluta repudiandae tempora quas, suscipit a necessitatibus. Consequatur, accusamus recusandae.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis deserunt veniam natus nobis! Maiores eligendi, aut culpa dolores quaerat soluta quisquam, illum voluptatibus, qui ducimus optio debitis labore nesciunt deserunt!
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem omnis ipsam voluptatibus repellat eum doloribus non distinctio a illum beatae voluptatem architecto aspernatur odio, corporis iste necessitatibus repellendus molestiae similique!
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit cum rerum eos eum placeat repellendus, odit error dolore corrupti sit laborum assumenda ipsa, aliquam, consequatur fuga quod ratione deserunt quos.
            </td>
            <td style="text-align: justify;" width="33%">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, harum est repellendus error ipsa incidunt nihil quasi iste optio quaerat! Nisi odio hic recusandae nobis, vitae libero totam ipsum culpa.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci fuga pariatur veniam sequi assumenda? Laborum rerum, suscipit facilis maiores dolorem aut veritatis? Debitis nesciunt officia cum, repellendus error itaque! Minima!
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio sint officia ipsum cum, quam sapiente itaque sunt ducimus vel illum reprehenderit ullam nisi, atque laboriosam enim. Illum ipsum odio labore?
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum ad nulla libero, odio dicta molestiae reiciendis ratione ipsam saepe error porro! Recusandae beatae facilis, numquam quas reiciendis quisquam iusto perferendis!
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero ex minima pariatur eos recusandae accusantium deserunt provident voluptates sit. Dolorum tenetur odio suscipit sunt, provident facilis ad dolor deserunt dolores!
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam fugit illum veniam veritatis minus repellat, velit earum, minima necessitatibus voluptatibus odit dignissimos ad et deleniti laborum exercitationem temporibus, vel distinctio.
            <td style="text-align: justify;" width="33%">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, harum est repellendus error ipsa incidunt nihil quasi iste optio quaerat! Nisi odio hic recusandae nobis, vitae libero totam ipsum culpa.
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Laborum necessitatibus sit minima nobis quaerat est. Dolore et voluptatibus aperiam dolor illum, quo aut excepturi optio beatae deleniti laboriosam soluta id!
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad assumenda magni ipsam numquam officia odit dolorum vitae animi illum ipsum! Asperiores similique hic sapiente dignissimos harum eaque quo, amet dolore?
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Aliquam, dicta dignissimos. Exercitationem, doloribus id alias dolorum asperiores excepturi inventore beatae consequatur temporibus ea possimus minima unde voluptas sint maxime non.
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo voluptatibus vel unde delectus, quia tenetur quasi nam suscipit, corporis dolores, quas reprehenderit. Delectus ea asperiores doloremque aliquam culpa beatae possimus?
            </td>
        </table>
    </article>
    <hr>
    <hr>
</body>
<footer style="background-image: url(./Imágenes/Cabecera2.jpg);">
    <div style="text-align: right;"><a href="./inicio.php"><img style="padding-right:20px;" src="./Imágenes/IFTS_logo.jpg"></a></div>
</footer>
</html>