<?php
include_once ('includes/Conexion.php');
$sqlus = "SELECT * FROM usuarios WHERE borrado = 0";
$sqlcar = "SELECT * FROM carreras";
$sqlmat = "SELECT * FROM materias";
$queryus = mysqli_query($conex, $sqlus);
$querycar = mysqli_query($conex, $sqlcar);
$querymat = mysqli_query($conex, $sqlmat);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"> <!--Uso UTF-8-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="icon" href="./Imágenes/IFTS_icono.ico" type="image/x-icon"> <!--Icono en la pestaña-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="includes/estilos.css" rel="stylesheet" type="text/css"> <!--Mira la hoja de estilos CSS-->
    <script language="javascript" src="includes/jquery-3.5.1.js"></script>
    <script language="javascript">
        $(document).ready(function(){
            $("#idcarrera").change(function(){
                $("#idcarrera option:selected") .each(function(){
                    idcarrera = $(this).val();
                    $.post("includes/CalculaMateriaXCarrera.php", {idcarrera: idcarrera
                    }, function(data){
                        $("#materia").html (data);
                    });
                });
            });
        });
    </script>
    <title>IFTS4 - Nueva Inscripción</title>
</head>
<body style="background-color: white;">
    <header style="background-image: url(./Imágenes/Cabecera2.jpg);">
        <a href="./inicio.php"><img style="padding-left:20px;" src="./Imágenes/IFTS_logo.jpg"></a>
    </header>
    <hr>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <table width="100%" frame="border">
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarAlumnos.php" style="text-decoration: none;color: #5B7354;">Alumnos</a></td></h2>
            </td>
            <td width ="25%">
                <h2 style="text-align: center;"><a href="./ListarCarreras.php" style="text-decoration: none; color: #5B7354">Carreras</a></h2>
            </td>
            <td width="25%" >
                <h2 style="text-align: center;"><a href="./ListarMaterias.php" style="text-decoration: none; color: #5B7354">Materias</a></h2>
            </td>
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarInscripciones.php" style="text-decoration: none;color: #5B7354;">Inscripciones</a></td></h2>
            </td>
        </table>
    </nav>
    
    <hr>
    <hr>
    <body style="background-color: rgb(206, 248, 250);">
    <h1 style="padding-left:20px;">Ingresar una nueva inscripción</h1>
    <hr>
    <hr>
    <form style="padding-left:20px;" action="includes/InsertaInscripcion.php" method="POST">
        <label for="alumno">Alumno</label><br>
            <select name="alumno" id="alumno">
                <option value="0">Seleccionar alumno...</option>
                <?php  while ($resus = $queryus->fetch_assoc()){?>
                <option value=<?php echo $resus ['id_usuario'] ?>><?php echo $resus ['nombre']; echo " "; echo $resus ['apellido']; echo " - "; echo $resus ['documento']; ?></option>
                <?php }?>
            </select>
        <br><br>
        <label for="idcarrera">Carrera</label><br>
            <select name="idcarrera" id="idcarrera">
            <option value="0">Seleccionar carrera...</option>
            <?php while ($rescar = $querycar->fetch_assoc()){?>
            <option value=<?php echo $rescar ['id_carrera'] ?>><?php echo $rescar ['descripcion'] ?></option>
            <?php }?>
            </select>  
        <br><br>
        <label for="materia">Materia</label><br>
            <select name="materia" id="materia"></select>
        <br><br>
        <button type="submit" class="btn btn-info" value="Actualizar">Agregar</button>
    </form>
    </body>   
    <hr>
    <hr>
</body>
<footer style="background-image: url(./Imágenes/Cabecera2.jpg);">
    <div style="text-align: right;"><a href="./inicio.php"><img style="padding-right:20px;" src="./Imágenes/IFTS_logo.jpg"></a></div>
</footer>
</html>