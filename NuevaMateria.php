<?php
include_once ('includes/Conexion.php');
$sql = "SELECT * FROM carreras";
$query = mysqli_query($conex,$sql);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"> <!--Uso UTF-8-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="icon" href="./Imágenes/IFTS_icono.ico" type="image/x-icon"> <!--Icono en la pestaña-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="includes/estilos.css" rel="stylesheet" type="text/css"> <!--Mira la hoja de estilos CSS-->
    <title>IFTS4 - Nueva materia</title>
</head>
<body style="background-color: white;">
    <header style="background-image: url(./Imágenes/Cabecera2.jpg);">
        <a href="./inicio.php"><img style="padding-left:20px;" src="./Imágenes/IFTS_logo.jpg"></a>
    </header>
    <hr>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <table width="100%" frame="border">
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarAlumnos.php" style="text-decoration: none;color: #5B7354;">Alumnos</a></td></h2>
            </td>
            <td width ="25%">
                <h2 style="text-align: center;"><a href="./ListarCarreras.php" style="text-decoration: none; color: #5B7354">Carreras</a></h2>
            </td>
            <td width="25%" >
                <h2 style="text-align: center;"><a href="./ListarMaterias.php" style="text-decoration: none; color: #5B7354">Materias</a></h2>
            </td>
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarInscripciones.php" style="text-decoration: none;color: #5B7354;">Inscripciones</a></td></h2>
            </td>
        </table>
    </nav>
    
    <hr>
    <hr>
    <body style="background-color: rgb(206, 248, 250);">
    <h1 style="padding-left:20px;">Ingresar una nueva materia</h1>
    <hr><hr>
    <form style="padding-left:20px;" action="includes/InsertaMateria.php" method="POST">
        <p>
            <label for="Materia">Descripción</label><br>
            <input name="Materia" type="text">
            <br><br>
            <label for="id_carrera">Seleccione la carrera de la que forma parte</label><br>
            <select name="id_carrera" id="id_carrera">
                <?php
                while ($res = $query->fetch_assoc()){?>
                <option value=<?php echo $res ['id_carrera'] ?>><?php echo $res ['descripcion'] ?></option>
                <?php } ?>
            </select>
            <br><br>
        </p>
        <button type="submit" class="btn btn-info" value="Actualizar">Agregar</button>
    </form>
    </body>   
    <hr>
    <hr>
</body>
<footer style="background-image: url(./Imágenes/Cabecera2.jpg);">
    <div style="text-align: right;"><a href="./inicio.php"><img style="padding-right:20px;" src="./Imágenes/IFTS_logo.jpg"></a></div>
</footer>
</html>