-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2020 a las 06:14:41
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ifts_site`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnorelcarreramateria`
--

CREATE TABLE `alumnorelcarreramateria` (
  `id_usuario` int(255) NOT NULL,
  `id_materia` int(255) NOT NULL,
  `id_carrera` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `alumnorelcarreramateria`
--

INSERT INTO `alumnorelcarreramateria` (`id_usuario`, `id_materia`, `id_carrera`) VALUES
(1, 1, 1),
(1, 3, 1),
(2, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `id_carrera` int(100) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id_carrera`, `descripcion`) VALUES
(1, 'Análisis de sistemas'),
(2, 'Tecnicatura en programación'),
(3, 'Administración de empresas'),
(4, 'Marketing'),
(5, 'Periodismo deportivo'),
(7, 'Abogacía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `id_materia` int(20) NOT NULL,
  `descripcion` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`id_materia`, `descripcion`) VALUES
(1, 'Desarrollo'),
(2, 'Redes'),
(3, 'Seguridad'),
(28, 'Seminario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiasrelcarreras`
--

CREATE TABLE `materiasrelcarreras` (
  `id_materia` int(255) NOT NULL,
  `id_carrera` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `materiasrelcarreras`
--

INSERT INTO `materiasrelcarreras` (`id_materia`, `id_carrera`) VALUES
(1, 1),
(2, 1),
(3, 1),
(28, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(20) NOT NULL,
  `documento` int(30) NOT NULL,
  `contraseña` varchar(250) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `calle` varchar(50) NOT NULL,
  `numero` int(10) NOT NULL,
  `piso` varchar(10) NOT NULL,
  `f_creacion` datetime NOT NULL,
  `habilitado` int(11) DEFAULT NULL,
  `borrado` int(11) DEFAULT NULL,
  `f_borrado` datetime DEFAULT NULL,
  `rol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `documento`, `contraseña`, `nombre`, `apellido`, `email`, `calle`, `numero`, `piso`, `f_creacion`, `habilitado`, `borrado`, `f_borrado`, `rol`) VALUES
(1, 40228439, 'inicio1', 'Santiago', 'Bermúdez', 'bsantiagoariel@gmail.com', 'Figueroa', 1200, 'PB', '0000-00-00 00:00:00', 1, 0, '0000-00-00 00:00:00', ''),
(2, 39664661, 'inicio1', 'Ailen', 'Mennella', 'ailenmennella@gmail.com', 'Pilcomayo', 1234, '1', '2020-11-26 23:59:10', 1, 0, NULL, ''),
(3, 30303030, 'inicio1', 'Pablo', 'Ingino', 'pablo.ingino@gmail.com', 'B', 1, '1', '2020-11-27 00:01:00', 1, 0, '2020-12-01 02:52:46', NULL),
(8, 12312312, '123123', 'Diego', 'Maradona', 'diego@maradona.com', 'Segurola y Habana', 4310, '7', '2020-12-01 16:26:25', 1, 0, NULL, 'Alumno'),
(9, 13464252, 'academia', 'Pepe', 'Argento', 'pepe@argento.com', 'Racing', 200, '2', '2020-12-01 16:27:23', 1, 0, NULL, 'Alumno'),
(10, 16464848, 'moniargento', 'Monica', 'Argento', 'moni@argento.com', 'Moni', 123, '1', '2020-12-01 16:40:48', 1, 0, '2020-12-01 16:40:52', 'Alumno');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnorelcarreramateria`
--
ALTER TABLE `alumnorelcarreramateria`
  ADD PRIMARY KEY (`id_usuario`,`id_materia`,`id_carrera`),
  ADD KEY `id_alumno` (`id_usuario`),
  ADD KEY `id_materia` (`id_materia`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`id_materia`);

--
-- Indices de la tabla `materiasrelcarreras`
--
ALTER TABLE `materiasrelcarreras`
  ADD PRIMARY KEY (`id_materia`,`id_carrera`),
  ADD KEY `id_materia` (`id_materia`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`,`documento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `id_carrera` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `materias`
--
ALTER TABLE `materias`
  MODIFY `id_materia` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnorelcarreramateria`
--
ALTER TABLE `alumnorelcarreramateria`
  ADD CONSTRAINT `alumnorelcarreramateria_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `alumnorelcarreramateria_ibfk_2` FOREIGN KEY (`id_carrera`) REFERENCES `carreras` (`id_carrera`) ON UPDATE CASCADE,
  ADD CONSTRAINT `alumnorelcarreramateria_ibfk_3` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id_materia`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `materiasrelcarreras`
--
ALTER TABLE `materiasrelcarreras`
  ADD CONSTRAINT `materiasrelcarreras_ibfk_1` FOREIGN KEY (`id_carrera`) REFERENCES `carreras` (`id_carrera`) ON UPDATE CASCADE,
  ADD CONSTRAINT `materiasrelcarreras_ibfk_2` FOREIGN KEY (`id_materia`) REFERENCES `materias` (`id_materia`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
