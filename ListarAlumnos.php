<?php
include_once ('includes/Conexion.php');
$sql = "SELECT * FROM usuarios WHERE borrado <> 1";
$query = mysqli_query($conex,$sql);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"> <!--Uso UTF-8-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="icon" href="./Imágenes/IFTS_icono.ico" type="image/x-icon"> <!--Icono en la pestaña-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="includes/estilos.css" rel="stylesheet" type="text/css"> <!--Mira la hoja de estilos CSS-->
    <title>IFTS4 - Alumnos</title>
</head>
<body style="background-color: white;">
    <header style="background-image: url(./Imágenes/Cabecera2.jpg);">
        <a href="./inicio.php"><img style="padding-left:20px;" src="./Imágenes/IFTS_logo.jpg"></a>
    </header>
    <hr>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <table width="100%" frame="border">
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarAlumnos.php" style="text-decoration: none;color: #5B7354;">Alumnos</a></td></h2>
            </td>
            <td width ="25%">
                <h2 style="text-align: center;"><a href="./ListarCarreras.php" style="text-decoration: none; color: #5B7354">Carreras</a></h2>
            </td>
            <td width="25%" >
                <h2 style="text-align: center;"><a href="./ListarMaterias.php" style="text-decoration: none; color: #5B7354">Materias</a></h2>
            </td>
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarInscripciones.php" style="text-decoration: none;color: #5B7354;">Inscripciones</a></td></h2>
            </td>
        </table>
    </nav>

    <hr><hr>
    <body style="background-color: rgb(206, 248, 250);">
    <h1>Lista de alumnos</h1>
    <hr><hr>
    <table border=1px align = center>
    <button style="padding-left:20px;" type="button" class="btn btn-success"><a href="NuevoAlumno.php" style="text-decoration: none; color: black;">Nuevo alumno</a></button>
        <thead >
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Documento</th>
            <th>Email</th>
            <th>Calle</th>
            <th>Numero</th>
            <th>Piso</th>
            <th>Habilitado</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </thead>
        <tbody >
            <?php
            while ($res = $query->fetch_assoc()){?>  
            <tr>
                <td><?php echo $res ['nombre'] ?></td>
                <td><?php echo $res ['apellido'] ?></td>
                <td><?php echo $res ['documento'] ?></td>
                <td><?php echo $res ['email'] ?></td>
                <td><?php echo $res ['calle'] ?></td>
                <td><?php echo $res ['numero'] ?></td>
                <td><?php echo $res ['piso'] ?></td>
                <td><?php switch ($res ['habilitado']){case 1: echo "Sí";break; case 0: echo "No";break;} ?></td>
                <td><a href="./ModificarUsuario.php?id=<?php echo $res['id_usuario'] ?>"><img src="./Imágenes/lapiz.png" width=22 height=22></a></td>
                <td><a href="includes/Borrausuario.php?id=<?php echo $res['id_usuario'] ?>"><img src="./Imágenes/borrar.png" width=22 height=22></a></td>
            </tr>
            <?php }?>
        </tbody>
        </table>
    </body>   
    <br>
    <hr>
    <hr>
</body>
<footer style="background-image: url(./Imágenes/Cabecera2.jpg);">
    <div style="text-align: right;"><a href="./inicio.php"><img style="padding-right:20px;" src="./Imágenes/IFTS_logo.jpg"></a></div>
</footer>
</html>
