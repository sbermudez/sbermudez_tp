<?php
include_once ('includes/Conexion.php');
$sqlcar = "SELECT * FROM carreras";
$querycar = mysqli_query($conex,$sqlcar);
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"> <!--Uso UTF-8-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="icon" href="./Imágenes/IFTS_icono.ico" type="image/x-icon"> <!--Icono en la pestaña-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="includes/estilos.css" rel="stylesheet" type="text/css"> <!--Mira la hoja de estilos CSS-->
    <title>IFTS4 - Carreras</title>
</head>
<body style="background-color: white;">
    <header style="background-image: url(./Imágenes/Cabecera2.jpg);">
        <a href="./inicio.php"><img style="padding-left:20px;" src="./Imágenes/IFTS_logo.jpg"></a>
    </header>
    <hr>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <table width="100%" frame="border">
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarAlumnos.php" style="text-decoration: none;color: #5B7354;">Alumnos</a></td></h2>
            </td>
            <td width ="25%">
                <h2 style="text-align: center;"><a href="./ListarCarreras.php" style="text-decoration: none; color: #5B7354">Carreras</a></h2>
            </td>
            <td width="25%" >
                <h2 style="text-align: center;"><a href="./ListarMaterias.php" style="text-decoration: none; color: #5B7354">Materias</a></h2>
            </td>
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarInscripciones.php" style="text-decoration: none;color: #5B7354;">Inscripciones</a></td></h2>
            </td>
        </table>
    </nav>

    <hr>
    <hr>
    <body style="background-color: rgb(206, 248, 250);">
    <h1>Lista de carreras</h1>
    <hr><hr>
    <table border = 1px align = "center">
    <button type="button" class="btn btn-success"><a href="NuevaCarrera.php" style="text-decoration: none; color: black;">Nueva carrera</a></button>
        <thead >
            <th>Carrera</th>
            <th>ID</th>
            <th>Editar</th>
            <th>Eliminar</th>
        </thead>
        <tbody style="text-align: center;">
            <?php
            while ($rescar = $querycar->fetch_assoc()){?>  
            <tr>
                <td><?php echo $rescar ['descripcion'] ?></td>
                <td><?php echo $rescar ['id_carrera'] ?></td>
                <td><a href="./ModificarCarrera.php?id=<?php echo $rescar['id_carrera'] ?>"><img src="./Imágenes/lapiz.png" width=22 height=22></a></td>
                <td><a href="includes/BorraCarrera.php?id=<?php echo $rescar['id_carrera'] ?>"><img src="./Imágenes/borrar.png" width=22 height=22></a></td>
            </tr>
            <?php }?>
        </tbody>
        </table>
        
    </body>   
    <br>
    <hr>
    <hr>
</body>
<footer style="background-image: url(./Imágenes/Cabecera2.jpg);">
    <div style="text-align: right;"><a href="./inicio.php"><img style="padding-right:20px;" src="./Imágenes/IFTS_logo.jpg"></a></div>
</footer>
</html>