<?php
include_once ('includes/Conexion.php');
$id = $_GET['id'];
$sql = "SELECT * FROM usuarios WHERE id_usuario = '$id'";
$query = mysqli_query($conex,$sql);

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"> <!--Uso UTF-8-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="icon" href="./Imágenes/IFTS_icono.ico" type="image/x-icon"> <!--Icono en la pestaña-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link href="includes/estilos.css" rel="stylesheet" type="text/css"> <!--Mira la hoja de estilos CSS-->
    <title>IFTS4 - Modificar usuario</title>
</head>
<body style="background-color: white;">
    <header style="background-image: url(./Imágenes/Cabecera2.jpg);">
        <a href="./inicio.php"><img style="padding-left:20px;" src="./Imágenes/IFTS_logo.jpg"></a>
    </header>
    <hr>
    <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
        <table width="100%" frame="border">
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarAlumnos.php" style="text-decoration: none;color: #5B7354;">Alumnos</a></td></h2>
            </td>
            <td width ="25%">
                <h2 style="text-align: center;"><a href="./ListarCarreras.php" style="text-decoration: none; color: #5B7354">Carreras</a></h2>
            </td>
            <td width="25%" >
                <h2 style="text-align: center;"><a href="./ListarMaterias.php" style="text-decoration: none; color: #5B7354">Materias</a></h2>
            </td>
            <td width= "25%">
                <h2 style="text-align: center;"><a href="./ListarInscripciones.php" style="text-decoration: none;color: #5B7354;">Inscripciones</a></td></h2>
            </td>
        </table>
    </nav>

    <hr>
    <hr>
    <body style="background-color: rgb(206, 248, 250);">
    <h1 style="padding-left:20px;">Modificar datos del usuario</h1>
    <hr>
    <form style="padding-left:20px;" action="includes/UpdateUsuario.php" method="POST">
    <?php
        while ($res = $query->fetch_assoc()){?>         
        <p>
            <label for="ID">ID</label>
            <input name="ID" type="number" value=<?php echo $id;?> readonly>
            <br><br>
            <label for="Nombre">Nombre</label><br>
            <input name="Nombre" type="text" value = <?php echo $res ['nombre'] ?>>
            <br><br>
            <label for="Apellido">Apellido</label><br>
            <input name="Apellido" type="text" value = <?php echo $res ['apellido'] ?>>
            <br><br>
            <label for="email">E-Mail</label><br>
            <input name="email" type="email" value = <?php echo $res ['email'] ?>>
            <br><br>
            <label for="DNI">DNI</label><br>
            <input name="DNI" type="number" value = <?php echo $res ['documento'] ?> >
            <br><br>
            <label for="Calle">Calle</label><br>
            <input name="Calle" type="text" value = <?php echo $res ['calle'] ?>>
            <br><br>
            <label for="Numero">Numero</label><br>
            <input name="Numero" type="number" value = <?php echo $res ['numero'] ?>>
            <br><br>
            <label for="Piso">Piso</label><br>
            <input name="Piso" type="text" value = <?php echo $res ['piso'] ?>>
            <br><br>
            <label for="Rol">Rol</label><br>
            <input name="Rol" type="text" value = <?php echo $res ['rol'] ?>>
            <br><br>
            Habilitado?<br>
            <select name="Habilitado" id="Habilitado">
                <option value="1">Sí</option>
                <option value="0">No</option>
            </select>
            <br>
        <?php }?>
        </p>
        <button type="submit" class="btn btn-info" value="Actualizar">Actualizar</button>
    </form>
    </body>   
    <hr>
    <hr>
</body>
<footer style="background-image: url(./Imágenes/Cabecera2.jpg);">
    <div style="text-align: right;"><a href="./inicio.php"><img style="padding-right:20px;" src="./Imágenes/IFTS_logo.jpg"></a></div>
</footer>
</html>